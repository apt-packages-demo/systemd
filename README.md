# systemd

Init system. https://tracker.debian.org/systemd

## Unofficial documentation
* [*Systemd*](https://en.m.wikipedia.org/wiki/Systemd)@WikipediA

## Ancillary components
### journald
#### Unofficial documentation
* [*Linux Logging with Systemd*
  ](https://www.loggly.com/ultimate-guide/linux-logging-with-systemd/)
  (2019) Sadequl Hussain

## Grapical interface
### systemd-manager
* https://crates.io/crates/systemd-manager
* https://repology.org/project/systemd-manager